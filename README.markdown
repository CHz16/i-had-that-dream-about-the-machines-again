i had that dream about the machines again
=========================================

A short, Twine-y text game, made for [6AMES MADE QUICK???](https://itch.io/jam/6ames-made-quick). Playable here: https://chz.itch.io/i-had-that-dream-about-the-machines-again

Everything here is by me and released under MIT (see LICENSE.txt), except:

**Fonts**

* [Breeze Sans, by Dalton Maag](https://developer.tizen.org/design/platform/styles/typography) ([Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0))
* [Fira Sans, by Carrois Apostrophe](https://fonts.google.com/specimen/Fira+Sans#about) ([SIL Open Font License](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL))
* [IBM Plex Sans, by Mike Abbink & Bold Monday](https://fonts.google.com/specimen/IBM+Plex+Sans#standard-styles) ([SIL Open Font License](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL))
* [Roboto, by Christian Robertson](https://fonts.google.com/specimen/Roboto#about) ([Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0))

**Images**

* `images/background1.jpg`, which is trimmed from ["Abstract Wallpaper" by lukeschmader"](https://www.flickr.com/photos/39222690@N04/3794648454) ([CC BY-SA 2.0](https://creativecommons.org/licenses/by-sa/2.0/), edit released under same license)
* `images/background2.jpg`, which is trimmed from ["Abstract abstract of an abstract" by Kevin Dooley](https://www.flickr.com/photos/12836528@N00/2853368756) ([CC BY 2.0](https://creativecommons.org/licenses/by/2.0/))
* `images/background3.jpg`, which is trimmed from ["Original Acrylic Abstract Painting on Canvas 'S6 LXVII'" by Carl Dunn](https://www.flickr.com/photos/132561248@N06/19005506079) ([CC BY-NC-SA 2.0](https://creativecommons.org/licenses/by-nc-sa/2.0/), edit released under same license)
* `images/background4.jpg`, which is trimmed from ["Abstract Fabric Swirls Background" by Philippa Willitts](https://www.flickr.com/photos/49503155381@N01/2496105788) ([CC BY-NC 2.0](https://creativecommons.org/licenses/by-nc/2.0/))
* `images/battery*.png`, which are edited from ["Battery" by Stockio from Flaticon](https://www.flaticon.com/premium-icon/battery_664883) ([Flaticon license](https://www.freepikcompany.com/legal#nav-flaticon))
* `images/camera-button*.png`, which are edited from ["Photo Camera Interface Symbol For Button" by Freepik from Flaticon](https://www.flaticon.com/free-icon/photo-camera-interface-symbol-for-button_45010) ([Flaticon license](https://www.freepikcompany.com/legal#nav-flaticon))
* `images/chat*.png`, which are ["Chat" by Smartline from Flaticon](https://www.flaticon.com/free-icon/chat_569412) ([Flaticon license](https://www.freepikcompany.com/legal#nav-flaticon))
* `images/coolpup69.jpg`, which is trimmed from ["Dog chillin' with red sunglasses" by rollanb](https://www.flickr.com/photos/rollanb/3545177630/) ([CC BY-SA 2.0](https://creativecommons.org/licenses/by-sa/2.0/), edit released under same license)
* `images/gear*.png`, which are edited from ["Gear" by Freepik from Flaticon](https://www.flaticon.com/premium-icon/gear_484613) ([Flaticon license](https://www.freepikcompany.com/legal#nav-flaticon))
* `images/lock*.png`, which are edited from ["Lock" by Those Icons from Flaticon](https://www.flaticon.com/free-icon/lock_483408) ([Flaticon license](https://www.freepikcompany.com/legal#nav-flaticon))
* `images/options*.png`, which are edited from ["Options" by wahya from Flaticon](https://www.flaticon.com/premium-icon/options_4044064) ([Flaticon license](https://www.freepikcompany.com/legal#nav-flaticon))
* `images/signal*.png`, which are edited from ["Signal" by Pixel perfect from Flaticon](https://www.flaticon.com/free-icon/signal_2313339) ([Flaticon license](https://www.freepikcompany.com/legal#nav-flaticon))
* `images/wifi*.png`, which are edited from ["Wifi" by Freepik from Flaticon](https://www.flaticon.com/free-icon/wifi_98783) ([Flaticon license](https://www.freepikcompany.com/legal#nav-flaticon))
* `images/bright-squares.png`, which is originally ["Bright Squares Pattern" by Waseem Dahman from Toptal Subtle Patterns](https://www.toptal.com/designers/subtlepatterns/bright-squares/) ([CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)). The image file here is [a transparent modification by Mike Hearn from Transparent Textures](https://www.transparenttextures.com/bright-squares.html).

**Audio**

* `sounds/lock.wav`, which is trimmed from ["DOOR PUSH IN LOCK.wav" by ThatMisfit from Freesound](https://freesound.org/people/ThatMisfit/sounds/388760/) ([CC0 1.0](http://creativecommons.org/publicdomain/zero/1.0/))
* `sounds/machine.wav`, which is edited from two sounds from Freesound: ["machine_rotation1.mp3" by Taira Komori](https://freesound.org/people/Taira%20Komori/sounds/213691/) ([CC BY 3.0](http://creativecommons.org/licenses/by/3.0/)) and ["Creepy old elevator 2" by bassboybg](https://freesound.org/people/bassboybg/sounds/218928/) ([CC0 1.0](http://creativecommons.org/publicdomain/zero/1.0/))
* `sounds/machineloop*`, which are ["machine_rotation1.mp3" by Taira Komori from Freesound](https://freesound.org/people/Taira%20Komori/sounds/213691/) ([CC BY 3.0](http://creativecommons.org/licenses/by/3.0/))
* `sounds/receive.wav`, which is converted from [`Receive.m4a` from the Adium sound set Elysium Express by Christian Cosas](https://www.adiumxtras.com/index.php?a=xtras&xtra_id=4812) ([CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/))
* `sounds/send.wav`, which is converted from [`Send.m4a` from the Adium sound set Elysium Express by Christian Cosas](https://www.adiumxtras.com/index.php?a=xtras&xtra_id=4812) ([CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/))
* `sounds/type.wav`, which is ["ui sound 1.wav" by nezuai from Freesound](https://freesound.org/people/nezuai/sounds/577020/) ([CC BY 3.0](http://creativecommons.org/licenses/by/3.0/))

**Code**

* `simplexNoise.js`, [an implementation of OpenSimplex noise by blindman67](https://github.com/blindman67/SimplexNoiseJS) ([Unlicense](http://unlicense.org))
* `smoothscroll.min.js`, [Smooth Scroll behavior polyfill by Dustan Kasten](https://github.com/iamdustan/smoothscroll) ([MIT](https://github.com/iamdustan/smoothscroll/blob/master/LICENSE))
