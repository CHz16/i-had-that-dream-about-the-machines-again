// main.js
// Initial UI setup and global behavior.


import { TEST_MODE } from "./globaldefinitions.js";

import { MusicPlayer } from "./musicplayer.js";
import { SoundPlayer } from "./soundplayer.js";

import { LoadingSceneController } from "./loadingscene.js";


//
// GLOBALS
//

export let audioContext;
export let soundPlayer, musicPlayer;

export let images = {};

export let scenes = [];
let currentScene = "loading";

let lastUpdateTime;


//
// INITIALIZATION
//

if (TEST_MODE) {
    let testCSS = document.createElement("link");
    testCSS.rel = "stylesheet";
    testCSS.type = "text/css";
    testCSS.href = "test.css";
    document.querySelector("head").appendChild(testCSS);

    let debugP = document.createElement("p");
    debugP.id = "debug";
    document.body.appendChild(debugP);
}

export function debug(s) {
    document.getElementById("debug").innerHTML += `<br />${s}`;
}


window.addEventListener("load", init);
function init() {
    audioContext = new (window.AudioContext || window.webkitAudioContext)();
    // resumed in the initial click handler on LoadingSceneController

    // While we don't save any user settings, Firefox will remember the volume sliders' positions on a refresh, so we'll read their values and set the players to match
    soundPlayer = new SoundPlayer({}, audioContext);
    soundPlayer.setVolume(document.getElementById("sfxvolumeslider").value);
    musicPlayer = new MusicPlayer({}, audioContext);
    musicPlayer.setVolume(document.getElementById("musicvolumeslider").value);

    scenes["loading"] = new LoadingSceneController();
    scenes["loading"].startTransitionIn();
    window.requestAnimationFrame(update);

    // All resource loading and other UI setup happens in LoadingSceneController
}

function update(timestamp) {
    if (lastUpdateTime === undefined) {
        lastUpdateTime = timestamp;
    }
    let dt = timestamp - lastUpdateTime;
    lastUpdateTime = timestamp;
    scenes[currentScene].update(timestamp, dt);

    window.requestAnimationFrame(update);
}



export function wait(time) {
    return new Promise(resolve => setTimeout(resolve, time));
}


export function switchToScene(scene) {
    if (currentScene !== undefined) {
        scenes[currentScene].startTransitionOut();
        let outgoingSceneContainer = document.getElementById(`${currentScene}scene`);
        outgoingSceneContainer.style.zIndex = 10;
        outgoingSceneContainer.classList.remove("fadescenein");
        outgoingSceneContainer.classList.add("fadesceneout");
    }
    currentScene = scene;
    scenes[currentScene].startTransitionIn();
    let incomingSceneContainer = document.getElementById(`${currentScene}scene`);
    incomingSceneContainer.style.zIndex = 20;
    incomingSceneContainer.classList.remove("fadesceneout");
    incomingSceneContainer.classList.add("fadescenein");
}
