// globaldefinitions.js
// Exported configuration constants


export function recursiveFreeze(obj) {
    for (let propertyName of Object.getOwnPropertyNames(obj)) {
        if (typeof obj[propertyName] == "object") {
            recursiveFreeze(obj[propertyName]);
        }
    }
    return Object.freeze(obj);
}

export const FADE_DURATION = 250; // milliseconds, coordinated with CSS fade animation


// Calculated constants
export const TEST_MODE = (window.location.protocol == "file:") && ((new URL(window.location)).searchParams.get("live") === null);
export const USING_FIREFOX = (window.navigator.userAgent.indexOf("Firefox") !== -1);
