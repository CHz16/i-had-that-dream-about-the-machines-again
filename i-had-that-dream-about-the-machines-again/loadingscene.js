// loadingscene.js
// Controller for the loading scene.


import { TEST_MODE } from "./globaldefinitions.js";
import { sounds, music, musicDefs, imageURLs, imageKeys, fonts } from "./mediadata.js";

import { SoundPreloader } from "./soundpreloader.js";
import { ImagePreloader } from "./imagepreloader.js";

import { audioContext, soundPlayer, musicPlayer, images, scenes, switchToScene } from "./main.js";

import { GameSceneController } from "./gamescene.js";
import { CreditsSceneController } from "./creditsscene.js";


const LOADING_PROGRESS_BAR_MAX_SPEED = (TEST_MODE ? 1 : 1 / 250); // % of the bar per millisecond, 1/1000 will fill the bar in 1 second


export class LoadingSceneController {
    constructor() {
        this.mediaLoaded = undefined, this.mediaCount = undefined, this.startedFonts = false;
        this.progress = undefined;
        this.errored = false;

        let handleInitialClick = function(event) {
            // so we can actually play audio later
            audioContext.resume();

            // Kick off the media loaders

            this.mediaLoaded = 0;
            this.mediaCount = sounds.length + music.length + imageURLs.length + fonts.length * 3;
            this.progress = 0;

            let soundLoadCallback = function(preloader, soundsHandled, index) {
                if (this.errored) {
                    return true;
                }
                this.mediaLoaded += 1;
                if (soundsHandled == preloader.sounds.length) {
                    soundPlayer.addBuffers(preloader.soundBuffers);
                }
            }.bind(this);
            let soundErrorCallback = function(preloader, soundsHandled, index) {
                this.errored = true;
                document.getElementById("errortext").textContent = `sound: ${preloader.sounds[index].key}`;
                document.getElementById("progressbox").style.display = "none";
                document.getElementById("errorbox").style.display = "block";
                return true;
            }.bind(this);
            let soundPreloader = new SoundPreloader(sounds, audioContext, {base: "sounds/", loadCallback: soundLoadCallback, errorCallback: soundErrorCallback});

            let musicLoadCallback = function(preloader, musicHandled, index) {
                if (this.errored) {
                    return true;
                }
                this.mediaLoaded += 1;
                if (musicHandled == preloader.sounds.length) {
                    let musicData = {};
                    for (let key of Object.keys(musicDefs)) {
                        let data = Object.assign({}, musicDefs[key]);
                        data["buffer"] = preloader.soundBuffers[key];
                        musicData[key] = data;
                    }
                    musicPlayer.addData(musicData);
                }
            }.bind(this);
            let musicErrorCallback = function(preloader, musicHandled, index) {
                this.errored = true;
                document.getElementById("errortext").textContent = `music: ${preloader.music[index].key}`;
                document.getElementById("progressbox").style.display = "none";
                document.getElementById("errorbox").style.display = "block";
                return true;
            }.bind(this);
            let musicPreloader = new SoundPreloader(music, audioContext, {base: "music/", loadCallback: musicLoadCallback, errorCallback: musicErrorCallback});

            let imageLoadCallback = function(preloader, imagesHandled, index) {
                if (this.errored) {
                    return true;
                }
                this.mediaLoaded += 1;
                images[imageKeys[index]] = preloader.getImage(imageKeys[index]);
                if (imagesHandled == imageURLs.length) {
                    // images done
                }
            }.bind(this);
            let imageErrorCallback = function(preloader, imagesHandled, index) {
                this.errored = true;
                document.getElementById("errortext").textContent = `image: ${imageKeys[index]}`;
                document.getElementById("progressbox").style.display = "none";
                document.getElementById("errorbox").style.display = "block";
                return true;
            }.bind(this);
            let imagePreloader = new ImagePreloader(imageURLs, {base: "images/", keys: imageKeys, loadCallback: imageLoadCallback, errorCallback: imageErrorCallback});

            // Now that that's done, we'll do other setup

            document.getElementById("loadingscene").removeEventListener("click", handleInitialClick);
            document.getElementById("loadingscene").style.cursor = "default";
            document.getElementById("clicktoloadbox").style.display = "none";
            document.getElementById("progressbox").style.display = "block";

            scenes["game"] = new GameSceneController();
            scenes["credits"] = new CreditsSceneController();
        }.bind(this);
        document.getElementById("loadingscene").addEventListener("click", handleInitialClick);
    }


    // main.js hooks

    update(timestamp, dt) {
        if (this.progress !== undefined) {
            // Move the progress bar forward according to the speed limit
            this.progress = Math.min(this.progress + dt * LOADING_PROGRESS_BAR_MAX_SPEED, this.mediaLoaded / this.mediaCount);
            document.getElementById("progress").value = this.progress;

            if (!this.startedFonts && this.mediaLoaded == this.mediaCount - fonts.length * 3) {
                this.startedFonts = true;
                for (let font of fonts) {
                    for (let weight of ["Regular", "Medium", "Thin"]) {
                        let fontFace = new FontFace(`${font}-${weight}`, `url(fonts/${font}/${font}-${weight}.ttf)`);
                        fontFace.load().then(function(loadedFontFace) {
                            document.fonts.add(loadedFontFace);
                            this.mediaLoaded += 1;
                        }.bind(this)).catch(function(error) {
                            this.errored = true;
                            document.getElementById("errortext").textContent = `font: ${font}-${weight}`;
                            document.getElementById("progressbox").style.display = "none";
                            document.getElementById("errorbox").style.display = "block";
                        }.bind(this));
                    }
                }
            }

            if (this.progress == 1) {
                switchToScene("game");
            }
        }
    }

    startTransitionIn() {

    }

    startTransitionOut() {

    }
}
