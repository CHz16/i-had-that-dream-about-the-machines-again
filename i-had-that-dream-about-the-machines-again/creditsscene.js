// creditsscene.js
// Controller for the credits scene.


import { fonts } from "./mediadata.js";

import { soundPlayer, wait } from "./main.js";

import { randomIntInRange } from "./numbers.js";


export class CreditsSceneController {
    constructor() {
        this.worldState = {};
    }


    // main.js hooks

    update(timestamp, dt) {

    }

    async startTransitionIn() {
        await wait(4000);
        this.rerollWorld();
    }

    startTransitionOut() {

    }


    // private

    async rerollWorld() {
        soundPlayer.play("machine");
        await wait(250);

        let keys = ["color", "font"];
        let customMaxes = { "color": 6 };
        for (let key of keys) {
            let currentValue = this.worldState[key] ?? -1;
            let newValue;
            do {
                newValue = randomIntInRange(1, customMaxes[key] ?? 4);
            } while (newValue == currentValue);
            this.worldState[key] = newValue;
        }

        let oldStylesheet = document.getElementById("worldstyle");
        if (oldStylesheet !== null) {
            oldStylesheet.parentNode.removeChild(oldStylesheet);
        }

        let newStylesheet = document.createElement("style");
        newStylesheet.id = "worldstyle";
        document.head.appendChild(newStylesheet);

        newStylesheet.sheet.insertRule(`#creditstext { display: flex; }`);
        let font = fonts[this.worldState["font"] - 1];
        newStylesheet.sheet.insertRule(`#creditstext { font-family: "${font}-Regular"; }`);

        if (this.worldState["color"] == 1) {
            newStylesheet.sheet.insertRule(`#creditstext { color: #FF4040; }`);
        } else if (this.worldState["color"] == 2) {
            newStylesheet.sheet.insertRule(`#creditstext { color: #40FF40; }`);
        } else if (this.worldState["color"] == 3) {
            newStylesheet.sheet.insertRule(`#creditstext { color: #A0A0FF; }`);
        } else if (this.worldState["color"] == 4) {
            newStylesheet.sheet.insertRule(`#creditstext { color: #FFFF00; }`);
        } else if (this.worldState["color"] == 5) {
            newStylesheet.sheet.insertRule(`#creditstext { color: #FF00FF; }`);
        } else if (this.worldState["color"] == 6) {
            newStylesheet.sheet.insertRule(`#creditstext { color: #00FFFF; }`);
        }

        window.setTimeout(this.rerollWorld.bind(this), 5000);
    }
}
