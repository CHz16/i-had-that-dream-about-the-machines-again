// gamescene.js
// Controller for the game scene.


import { fonts } from "./mediadata.js";
import * as script from "./script.js";

import { soundPlayer, musicPlayer, switchToScene, wait } from "./main.js";

import { recursiveFreeze, FADE_DURATION, TEST_MODE, USING_FIREFOX } from "./globaldefinitions.js";
import { randomIntInRange } from "./numbers.js";
import { openSimplexNoise } from "./simplexNoise.js";


const SHAKE_MAX_X_OFFSET = 20, SHAKE_MAX_Y_OFFSET = 8;
const SHAKE_DECAY = 2 * (1 / 1000);

const MessagingState = {waitingToStart: 1, typing: 2, waitingToSend: 3, receiving: 4, checkScrollPosition: 5, readyForNextMessage: 6, waitingToFinish: 999};
recursiveFreeze(MessagingState);

// all milliseconds
const BLOCK_START_DELAY = 1000
const MESSAGE_START_DELAY = 550, SWITCH_DELAY = 1000;
const TYPING_DELAY = 100, SEND_DELAY = 200;
const FINISH_DELAY = 2000;
const RESTART_AFTER_SCROLLING_DELAY = 500;



export class GameSceneController {
    constructor() {
        // property initialization

        this.worldState = {};
        this.noise = undefined;
        this.shakeLevel = 0;
        this.noiseX = undefined;
        this.startTime = new Date(Date.now() - 60000);
        this.firstCommand = true;
        this.lowercase = false;
        this.currentSectionIndex = 0;
        this.incomingScript = undefined;
        this.optionsShowing = false;
        this.optionsBoxMouseDowned = undefined;
        this.playingSFXTest = false;
        this.messagingState = undefined;
        this.stateTransitionCountdown = undefined;
        this.lastSent = undefined;
        this.remainingCharacters = undefined;
        this.waitingForScroll = false;
        this.scrolledToBottom = undefined, this.lastScrollTop = 0;
        this.overlap = false, this.deletingLastMessage = false;
        this.overlapStartCountdown = undefined, this.overlapSendCountdown = undefined;

        // setting up events

        let handleUnlock = function(event) {
            document.getElementById("command1link").removeEventListener("click", handleUnlock);
            this.transitionToMessages();
        }.bind(this);
        document.getElementById("command1link").addEventListener("click", handleUnlock);

        document.getElementById("optionsbutton").addEventListener("click", async function(event) {
            document.getElementById("options").style.display = "flex";
            this.fadeInElement("options");
            this.optionsShowing = true;
            await wait(FADE_DURATION);
        }.bind(this));
        // Do some funky click tracking here because in Safari, if you drag a slider and end the drag in the outer opacity layer, it counts as a click on the outer options box, which isn't great
        document.getElementById("options").addEventListener("mousedown", function(event) {
            this.optionsBoxMouseDowned = false;
        }.bind(this));
        document.getElementById("options").addEventListener("click", async function(event) {
            if (!this.optionsShowing || this.optionsBoxMouseDowned) {
                return;
            }
            this.fadeOutElement("options");
            await wait(FADE_DURATION);
            document.getElementById("options").style.display = "none";
            this.optionsShowing = false;
        }.bind(this));
        document.getElementById("optionsbox").addEventListener("mousedown", function(event) {
            this.optionsBoxMouseDowned = true;
            event.stopImmediatePropagation();
        }.bind(this));

        document.getElementById("sfxvolumeslider").addEventListener("input", function(event) {
            if (!this.playingSFXTest) {
                this.playingSFXTest = true;
                soundPlayer.play("machineloop", { loop: true, exclusive: true });
            }
            soundPlayer.setVolume(event.target.value);
        }.bind(this));
        document.getElementById("sfxvolumeslider").addEventListener("change", function(event) {
            this.playingSFXTest = false;
            soundPlayer.fadeOut("machineloop", 0.25);
        }.bind(this));

        document.getElementById("musicvolumeslider").addEventListener("input", function(event) {
            musicPlayer.setVolume(event.target.value);
        }.bind(this));

        document.getElementById("messages").addEventListener("scroll", async function(event) {
            if (this.scrolledToBottom) {
                if (this.lastScrollTop > event.target.scrollTop && !this.checkScrolledToBottom()) {
                    this.scrolledToBottom = false;
                }
            } else {
                if (this.checkScrolledToBottom()) {
                    this.scrolledToBottom = true;
                }
            }
            // Block separated out instead of in the else branch above, since if we click the scroll indicator, we'll hit the bottom while this.scrolledToBottom is true, so this check needs to happen for both branches
            if (this.waitingForScroll && this.checkScrolledToBottom()) {
                this.waitingForScroll = false;
                this.messagingState = MessagingState.readyForNextMessage;
                this.stateTransitionCountdown = RESTART_AFTER_SCROLLING_DELAY;
                this.fadeOutElement("scrollindicator");
                await wait(FADE_DURATION);
                document.getElementById("scrollindicator").style.display = "none";
            }
            this.lastScrollTop = event.target.scrollTop;
        }.bind(this));
        document.getElementById("messages").addEventListener("click", function(event) {
            if (this.waitingForScroll && event.offsetY >= (event.target.offsetHeight - document.getElementById("scrollindicator").offsetHeight)) {
                this.scrollToBottom(true);
            } else if (this.scrolledToBottom) {
                // Fast forward
                if (this.messagingState == MessagingState.waitingToStart) {
                    this.stateTransitionCountdown = -1;
                } else if (this.messagingState == MessagingState.receiving) {
                    this.stateTransitionCountdown = -1;
                } else if (this.messagingState == MessagingState.typing) {
                    // largely copied from GameSceneController.update
                    if (this.overlapStartCountdown !== undefined) {
                        this.overlapStartCountdown = undefined;
                        document.getElementById("textinput").textContent += this.remainingCharacters.join("");
                        this.remainingCharacters = undefined;
                        this.messagingState = MessagingState.waitingToStart;
                        this.stateTransitionCountdown = -1;
                        this.overlap = false;
                        this.deletingLastMessage = true;
                    } else if (this.overlapSendCountdown !== undefined) {
                        this.overlapSendCountdown = undefined;
                        document.getElementById("textinput").textContent += this.remainingCharacters.join("");
                        this.remainingCharacters = undefined;
                        document.getElementById("messages").lastChild.querySelector("p").textContent = this.incomingScript[0].substring(2);
                        soundPlayer.play("receive");
                        this.incomingScript.shift();
                        this.messagingState = MessagingState.waitingToStart;
                        this.stateTransitionCountdown = -1;
                        this.overlap = false;
                        this.deletingLastMessage = true;
                    } else {
                        if (this.remainingCharacters[0] == "del") {
                            this.remainingCharacters = this.remainingCharacters.filter(x => x != "del");
                            document.getElementById("textinput").textContent = "";
                        }
                        document.getElementById("textinput").textContent += this.remainingCharacters.join("");
                        this.remainingCharacters = undefined;
                        this.messagingState = MessagingState.waitingToSend;
                    }
                    this.stateTransitionCountdown = -1;
                } else if (this.messagingState == MessagingState.waitingToFinish) {
                    this.stateTransitionCountdown = -1;
                }
            }
        }.bind(this));

        // game contents

        document.getElementById("lockscreennotificationheadertime").textContent = (new Intl.DateTimeFormat([], { timeStyle: "short" })).format(this.startTime);

        let timestamps = script.preludeDesiredTimestamps.map(function(spec) {
            let timestamp = new Date(this.startTime - spec.daysAgo * 24 * 60 * 60 * 1000);
            timestamp.setHours(spec.hours, spec.minutes);
            return timestamp;
        }.bind(this));
        let timestampFormatter = new Intl.DateTimeFormat([], { dateStyle: "short", timeStyle: "short" });
        for (let i = 0; i < script.prelude.length; i++) {
            let timestampLine = document.createElement("p");
            timestampLine.className = "timestamp";
            timestampLine.textContent = timestampFormatter.format(timestamps[i]);
            document.getElementById("messages").appendChild(timestampLine);
            for (let message of script.prelude[i]) {
                let messageLine;
                if (message[0] == "<") {
                    messageLine = document.getElementById("senttemplate").content.firstElementChild.cloneNode(true);
                } else {
                    messageLine = document.getElementById("receivedtemplate").content.firstElementChild.cloneNode(true);
                }
                messageLine.querySelector("p").textContent = message.substring(2);
                document.getElementById("messages").appendChild(messageLine);
            }
        }
    }


    // main.js hooks

    update(timestamp, dt) {
        let now = new Date();
        let timeString = (new Intl.DateTimeFormat([], { timeStyle: "short" })).format(now);

        document.getElementById("lockscreentime").textContent = timeString;
        document.getElementById("lockscreendate").textContent = (new Intl.DateTimeFormat([], { weekday: "long", month: "long", day: "numeric" })).format(now);

        document.getElementById("messagesscreentime").textContent = timeString;

        if (this.noise !== undefined) {
            if (this.shakeLevel > 0) {
                let power = this.shakeLevel * this.shakeLevel;
                let xOffset = SHAKE_MAX_X_OFFSET * power * this.noise.noise2D(this.noiseX, 0);
                let yOffset = SHAKE_MAX_Y_OFFSET * power * this.noise.noise2D(this.noiseX, 1);
                document.getElementById("phoneshakebox").style.left = `${xOffset}px`;
                document.getElementById("phoneshakebox").style.top = `${yOffset}px`;

                this.noiseX += 1;
                this.shakeLevel = this.shakeLevel - Math.max(0, SHAKE_DECAY * dt);
            } else {
                this.noise = undefined;
                this.noiseX = undefined;
                document.getElementById("phoneshakebox").style.left = 0;
                document.getElementById("phoneshakebox").style.top = 0;
            }
        }

        if (TEST_MODE) {
            document.getElementById("debug").innerHTML = `${this.messagingState}<br />${this.stateTransitionCountdown}<br />${this.scrolledToBottom ? "true" : "false"}`;
        }

        if (this.messagingState === undefined || this.optionsShowing) {
            return;
        }
        this.stateTransitionCountdown -= dt * document.getElementById("typingspeedslider").value;
        if (this.stateTransitionCountdown > 0) {
            return;
        }

        if (this.messagingState == MessagingState.waitingToStart) {
            if (this.incomingScript[0][0] == "<") {
                this.messagingState = MessagingState.typing;
                this.stateTransitionCountdown = TYPING_DELAY;
                this.lastSent = true;
                let line = this.incomingScript[0].substring(2);
                if (this.overlap) {
                    line = line.substring(0, 14);
                }
                if (this.lowercase) {
                    line = line.toLowerCase();
                }
                if (!this.deletingLastMessage) {
                    this.remainingCharacters = [...line];
                } else {
                    this.deletingLastMessage = false;
                    this.remainingCharacters = Array([...document.getElementById("textinput").textContent].length).fill("del").concat([...line]);
                }
                this.incomingScript.shift();
            } else {
                this.messagingState = MessagingState.receiving;
                this.lastSent = false;
                // copied in .typing block for overlap
                let messageLine = document.getElementById("receivedtemplate").content.firstElementChild.cloneNode(true);
                messageLine.querySelector("p").textContent = "• • •";
                document.getElementById("messages").appendChild(messageLine);
                if (this.scrolledToBottom) {
                    this.scrollToBottom(true);
                }
                this.stateTransitionCountdown = TYPING_DELAY * ([...this.incomingScript[0]].length - 2) + SEND_DELAY;
            }
        } else if (this.messagingState == MessagingState.typing) {
            if (document.getElementById("textinput").textContent == "\u00A0") {
                document.getElementById("textinput").textContent = "";
            }
            if (this.remainingCharacters[0] == "del") {
                document.getElementById("textinput").textContent = [...document.getElementById("textinput").textContent].slice(0, -1).join("");
                if (document.getElementById("textinput").textContent.length == 0) {
                    document.getElementById("textinput").textContent = "\u00A0";
                }
            } else {
                document.getElementById("textinput").textContent += this.remainingCharacters[0];
            }
            if (this.scrolledToBottom) {
                this.scrollToBottom();
            }
            this.remainingCharacters.shift();
            soundPlayer.play("type");

            // a lot of this overlap funkiness also had to be accounted for in the #messages.click handler
            if (this.overlapStartCountdown !== undefined) {
                this.overlapStartCountdown -= 1;
                if (this.overlapStartCountdown == 0) {
                    this.overlapStartCountdown = undefined;
                    this.overlapSendCountdown = [...this.incomingScript[0]].length - 2;
                    // copied from .waitingToStart block
                    let messageLine = document.getElementById("receivedtemplate").content.firstElementChild.cloneNode(true);
                    messageLine.querySelector("p").textContent = "• • •";
                    document.getElementById("messages").appendChild(messageLine);
                    if (this.scrolledToBottom) {
                        this.scrollToBottom(true);
                    }
                }
            } else if (this.overlapSendCountdown !== undefined) {
                this.overlapSendCountdown -= 1;
                if (this.overlapSendCountdown == 0) {
                    this.overlapSendCountdown = undefined;
                    // copied from .receiving block
                    document.getElementById("messages").lastChild.querySelector("p").textContent = this.incomingScript[0].substring(2);
                    soundPlayer.play("receive");
                    if (this.scrolledToBottom) {
                        this.scrollToBottom(true);
                    }
                    this.incomingScript.shift();
                }
            }

            if (this.remainingCharacters.length > 0) {
                this.stateTransitionCountdown = TYPING_DELAY;
            } else {
                if (!this.overlap) {
                    this.remainingCharacters = undefined;
                    this.messagingState = MessagingState.waitingToSend;
                    this.stateTransitionCountdown = SEND_DELAY;
                } else {
                    this.overlap = false;
                    this.deletingLastMessage = true;
                    this.messagingState = MessagingState.checkScrollPosition;
                }
            }
        } else if (this.messagingState == MessagingState.waitingToSend) {
            let messageLine = document.getElementById("senttemplate").content.firstElementChild.cloneNode(true);
            messageLine.querySelector("p").textContent = document.getElementById("textinput").textContent;
            document.getElementById("messages").appendChild(messageLine);
            soundPlayer.play("send");
            document.getElementById("textinput").textContent = "\u00A0";
            if (this.scrolledToBottom) {
                this.scrollToBottom(true);
            }
            this.messagingState = MessagingState.checkScrollPosition
        } else if (this.messagingState == MessagingState.receiving) {
            // copied in .typing block for overlap
            document.getElementById("messages").lastChild.querySelector("p").textContent = this.incomingScript[0].substring(2);
            soundPlayer.play("receive");
            if (this.scrolledToBottom) {
                this.scrollToBottom(true);
            }
            this.incomingScript.shift();
            this.messagingState = MessagingState.checkScrollPosition;
        } else if (this.messagingState == MessagingState.checkScrollPosition) {
            if (!this.scrolledToBottom && (this.incomingScript.length == 0 || (this.incomingScript[0][0] == "<" && !this.lastSent))) {
                this.messagingState = undefined;
                this.stateTransitionCountdown = undefined;
                this.waitingForScroll = true;
                document.getElementById("scrollindicator").style.display = "block";
                this.fadeInElement("scrollindicator");
            } else {
                this.messagingState = MessagingState.readyForNextMessage;
            }
        } else if (this.messagingState == MessagingState.readyForNextMessage) {
            if (this.incomingScript.length > 0) {
                this.messagingState = MessagingState.waitingToStart;
                this.stateTransitionCountdown = MESSAGE_START_DELAY;
                if ((this.incomingScript[0][0] == "<" && !this.lastSent) || (this.incomingScript[0][0] == ">" && this.lastSent)) {
                    this.stateTransitionCountdown += SWITCH_DELAY;
                }
            } else {
                this.messagingState = MessagingState.waitingToFinish;
                this.stateTransitionCountdown = FINISH_DELAY;
            }
        } else if (this.messagingState == MessagingState.waitingToFinish) {
            let prompts = script.sections[this.currentSectionIndex].prompts;
            if (prompts.length > 0) {
                if (this.lowercase) {
                    prompts = prompts.map(prompt => prompt.toLowerCase());
                }
                document.getElementById("command1link").textContent = prompts[0];
                document.getElementById("command2link").textContent = prompts[1];
            } else {
                document.getElementById("command1link").textContent = "(lock phone)";
                document.getElementById("command2").style.display = "none";
            }
            this.showCommands();
            this.messagingState = undefined;
            this.stateTransitionCountdown = undefined;
            this.lastSent = undefined;
        }
    }

    startTransitionIn() {
        this.rerollWorld(false);
    }

    startTransitionOut() {

    }


    // private

    async rerollWorld(effects = true) {
        if (effects) {
            soundPlayer.play("machine");
            await wait(250);
            this.shakeLevel = 1;
            this.noise = openSimplexNoise(Date.now());
            this.noiseX = 0;
        }

        let keys = ["music", "buttons", "x", "y", "cornerRadius", "camera", "background", "font", "notificationColor", "lockScreenSpacerHeight", "statusBarOrder", "statusBarIconsOrder", "transitionDirection", "messageColor"];
        let customMaxes = { "statusBarOrder": 2, "statusBarIconsOrder": 2, "messageColor": 3 };
        for (let key of keys) {
            let currentValue = this.worldState[key] ?? -1;
            let newValue;
            do {
                newValue = randomIntInRange(1, customMaxes[key] ?? 4);
            } while (newValue == currentValue);
            this.worldState[key] = newValue;
        }

        let oldStylesheet = document.getElementById("worldstyle");
        if (oldStylesheet !== null) {
            oldStylesheet.parentNode.removeChild(oldStylesheet);
        }

        let newStylesheet = document.createElement("style");
        newStylesheet.id = "worldstyle";
        document.head.appendChild(newStylesheet);

        musicPlayer.play(`${this.worldState["music"]}`, {loop: true, sync: true, fadeDuration: 0});

        document.getElementById("phonebuttons").src = `images/buttons${this.worldState["buttons"]}.png`;
        newStylesheet.sheet.insertRule(`#phone { left: ${-4 + this.worldState["x"] * 6}px; }`);
        newStylesheet.sheet.insertRule(`#phone { top: ${-4 + this.worldState["y"] * 8}px; }`);

        newStylesheet.sheet.insertRule(`#phonecontents { border-radius: ${10 * this.worldState["cornerRadius"]}px; }`);
        newStylesheet.sheet.insertRule(`#phoneborderoverlay { border-radius: ${10 * this.worldState["cornerRadius"]}px; }`);
        if (this.worldState["cornerRadius"] == 4) {
            // round off the top corners to remove visible artifacts with the largest radius
            newStylesheet.sheet.insertRule(`#messagesscreentopbar { border-radius: 10px 10px 0 0; }`);
        }

        document.getElementById("phonecamera").src = `images/camera${this.worldState["camera"]}.png`;
        document.getElementById("phonecamera").srcset = `images/camera${this.worldState["camera"]}@2x.png 2x`;
        if (this.worldState["camera"] == 4) {
            // left camera, shove status bar left to compensate
            newStylesheet.sheet.insertRule(".statusbar { padding-left: 40px; }");
        }
        newStylesheet.sheet.insertRule(`#phonecontents { background-image: url(images/background${this.worldState["background"]}.jpg); background-size: contain; }`);

        let font = fonts[this.worldState["font"] - 1];
        newStylesheet.sheet.insertRule(`#phonecontents { font-family: "${font}-Regular"; }`);
        newStylesheet.sheet.insertRule(`.thin { font-family: "${font}-Thin"; }`);
        newStylesheet.sheet.insertRule(`.medium { font-family: "${font}-Medium"; }`);

        let r = 255, g = 255, b = 255;
        if (this.worldState["notificationColor"] == 1) {
            r = 224;
            g = 224;
        } else if (this.worldState["notificationColor"] == 2) {
            g = 224;
            b = 224;
        } else if (this.worldState["notificationColor"] == 3) {
            b = 224;
            r = 224;
        }
        newStylesheet.sheet.insertRule(`#lockscreennotification { background-color: rgba(${r}, ${g}, ${b}, 0.85); }`);

        newStylesheet.sheet.insertRule(`#lockscreenspacer { height: ${ -70 + 70 * this.worldState["lockScreenSpacerHeight"]}px; }`);
        if (this.worldState["statusBarOrder"] == 1) {
            newStylesheet.sheet.insertRule(".statusbar { flex-direction: row; }");
        } else {
            newStylesheet.sheet.insertRule(".statusbar { flex-direction: row-reverse; }");
        }
        if (this.worldState["statusBarIconsOrder"] == 1) {
            newStylesheet.sheet.insertRule(".statusbaricons { flex-direction: row; }");
        } else {
            newStylesheet.sheet.insertRule(".statusbaricons { flex-direction: row-reverse; }");
        }

        if (this.worldState["messageColor"] == 1) {
            newStylesheet.sheet.insertRule(`.received > p { background-color: #FFE0E0; }`);
            newStylesheet.sheet.insertRule(`.received > .arrow { border-right-color: #FFE0E0; }`);
            newStylesheet.sheet.insertRule(`.sent > p { background-color: #D00000; }`);
            newStylesheet.sheet.insertRule(`.sent > .arrow { border-left-color: #D00000; }`);
            newStylesheet.sheet.insertRule(`#mediabutton { background-color: #D00000; }`);
            newStylesheet.sheet.insertRule(`#textinput { background-color: #D00000; }`);
            newStylesheet.sheet.insertRule(`#textinputarrow { border-left-color: #D00000; }`);
        } else if (this.worldState["messageColor"] == 2) {
            newStylesheet.sheet.insertRule(`.received > p { background-color: #E0FFE0; }`);
            newStylesheet.sheet.insertRule(`.received > .arrow { border-right-color: #E0FFE0; }`);
            newStylesheet.sheet.insertRule(`.sent > p { background-color: #00D000; }`);
            newStylesheet.sheet.insertRule(`.sent > .arrow { border-left-color: #00D000; }`);
            newStylesheet.sheet.insertRule(`#mediabutton { background-color: #00D000; }`);
            newStylesheet.sheet.insertRule(`#textinput { background-color: #00D000; }`);
            newStylesheet.sheet.insertRule(`#textinputarrow { border-left-color: #00D000; }`);
        } else if (this.worldState["messageColor"] == 3) {
            newStylesheet.sheet.insertRule(`.received > p { background-color: #E0E0FF; }`);
            newStylesheet.sheet.insertRule(`.received > .arrow { border-right-color: #E0E0FF; }`);
            newStylesheet.sheet.insertRule(`.sent > p { background-color: #0000D0; }`);
            newStylesheet.sheet.insertRule(`.sent > .arrow { border-left-color: #0000D0; }`);
            newStylesheet.sheet.insertRule(`#mediabutton { background-color: #0000D0; }`);
            newStylesheet.sheet.insertRule(`#textinput { background-color: #0000D0; }`);
            newStylesheet.sheet.insertRule(`#textinputarrow { border-left-color: #0000D0; }`);
        }

        if (this.scrolledToBottom) {
            this.scrollToBottom();
        }
    }

    async transitionToMessages() {
        this.rerollWorld()
        this.hideCommands();
        await wait(2000);

        document.getElementById("messagesscreen").style.display = "flex";
        this.scrollToBottom();
        if (this.worldState["transitionDirection"] == 1) {
            document.getElementById("lockscreen").classList.add("movelockscreenleft");
            document.getElementById("messagesscreen").classList.add("movemessagesscreenleft");
        } else if (this.worldState["transitionDirection"] == 2) {
            document.getElementById("lockscreen").classList.add("movelockscreenright");
            document.getElementById("messagesscreen").classList.add("movemessagesscreenright");
        } else if (this.worldState["transitionDirection"] == 3) {
            document.getElementById("lockscreen").classList.add("movelockscreenup");
            document.getElementById("messagesscreen").classList.add("movemessagesscreenup");
        } else {
            document.getElementById("lockscreen").classList.add("movelockscreendown");
            document.getElementById("messagesscreen").classList.add("movemessagesscreendown");
        }

        await wait(500);
        document.getElementById("lockscreen").style.display = "none";
        document.getElementById("lockscreenstatusbar").style.display = "none";

        await wait(2000);
        document.getElementById("command1link").textContent = script.sections[0].prompts[0];
        document.getElementById("command1link").addEventListener("click", function(event) {
            this.chooseCommand(1);
        }.bind(this));
        document.getElementById("command2").style.display = "block";
        document.getElementById("command2link").textContent = script.sections[0].prompts[1];
        document.getElementById("command2link").addEventListener("click", function(event) {
            this.chooseCommand(2);
        }.bind(this));
        this.showCommands();
    }

    showCommands() {
        document.getElementById("commandshield").style.zIndex = -1;
        document.getElementById("commands").style.display = "block";
        this.fadeInElement("commands");
    }

    async hideCommands() {
        document.getElementById("commandshield").style.zIndex = 100;
        this.fadeOutElement("commands");
        await wait(FADE_DURATION);
        document.getElementById("commands").style.display = "none";
    }

    async chooseCommand(command) {
        this.hideCommands();
        await this.rerollWorld();

        if (this.firstCommand) {
            this.firstCommand = false;
            if (command == 2) {
                this.lowercase = true;
                for (let message of document.querySelectorAll(".sent > p")) {
                    message.textContent = message.textContent.toLowerCase();
                }
            }
        }

        if (this.currentSectionIndex == script.sections.length - 1) {
            this.endGame();
            return;
        }

        this.incomingScript = [`< ${script.sections[this.currentSectionIndex].prompts[command - 1]}`].concat(script.sections[this.currentSectionIndex].endings[command - 1]).concat(script.sections[this.currentSectionIndex + 1].body);
        if (this.lowercase) {
            this.incomingScript = this.incomingScript.map(message => message.toLowerCase());
        }
        this.currentSectionIndex += 1;

        this.messagingState = MessagingState.waitingToStart;
        this.stateTransitionCountdown = BLOCK_START_DELAY;
        this.lastSent = true;

        if (this.currentSectionIndex == script.sections.length - 1) {
            this.overlap = true;
            this.overlapStartCountdown = 2;
        }
    }

    async endGame() {
        document.getElementById("optionsbutton").style.display = "none";

        await wait(1000);
        document.getElementById("phone").classList.add("movephoneaway");
        await wait(3500);
        soundPlayer.play("lock");
        musicPlayer.stop(0);
        document.getElementById("black").style.display = "block";
        switchToScene("credits");
    }

    scrollToBottom(animated = false) {
        let messages = document.getElementById("messages");
        // Embedded on itch.io, using messages.scrollHeight actually scrolls down the entire page when sending long messages (like 3+ lines), through the iframe, which is great and not even the tiniest bit what I want to happen
        // This calculation seems to fix it, maybe
        let scrollTop = USING_FIREFOX ? messages.scrollHeight - messages.clientHeight : messages.scrollHeight;
        messages.scrollTo({ left: 0, top: scrollTop, behavior: (animated ? "smooth" : "auto") });
        this.scrolledToBottom = true;
    }

    checkScrolledToBottom() {
        let messages = document.getElementById("messages");
        return (messages.scrollTop + messages.clientHeight >= messages.scrollHeight);
    }

    fadeInElement(element) {
        if (typeof element == "string") {
            element = document.getElementById(element);
        }
        element.classList.remove("fadesceneout");
        element.classList.add("fadescenein");
    }

    fadeOutElement(element) {
        if (typeof element == "string") {
            element = document.getElementById(element);
        }
        element.classList.remove("fadescenein");
        element.classList.add("fadesceneout");
    }
}
