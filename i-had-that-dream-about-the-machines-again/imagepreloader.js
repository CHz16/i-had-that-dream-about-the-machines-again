// imagepreloader.js
// Object for easy preloading of images, with event hooks for actions to take
// place after loading.

// This class preloads the list of images whose URLs are given in the array
// urls. The second optional argument is an object with any or all of the
// following properties set:
//
//   - base: A base directory for image URLs. If this property is set, then the
//           image at url[i] will be fetched from (base + url[i]).
//
//   - keys: A list of keys to be used to access the images in the preloader.
//           The first string in keys will key to the first image in urls, and
//           so on. If this property is not set, then the URLs themselves from
//           the urls array (not base + url) will be used as the keys.
//
//   - loadCallback: A function to call after any image is loaded. The function
//                   signature should look like this:
//                       function loadCallback(preloader, imagesHandled, index)
//                   preloader is the preloader object, imagesHandled is the
//                   total number of images that have loaded or errored, and
//                   index is the index of the image in urls that was just
//                   loaded. This function does not need a return value, but if
//                   it returns true, then no further images will be loaded.
//
//   - errorCallback: A function to call if there's an error in loading an
//                    image. The function signature should look like this:
//                        function errorCallback(pl, imagesHandled, index)
//                    pl is the preloader object, imagesHandled is the total
//                    number of images that have loaded or errored, and index is
//                    the index of the image in urls that just failed to load.
//                    This function does not need a return value, but if it
//                    returns true, then no further images will be loaded.
//
//   - allCallback: A function to call after all images have either loaded or
//                  errored out. The function signature should look like this:
//                      function allCallback(preloader, imagesHandled, errors)
//                  preloader is the preloader object, imagesHandled is the
//                  total number of images that were attempted to be loaded, and
//                  errors is an array containing the keys of all the images
//                  that were unable to be loaded. This callback will be called
//                  AFTER loadCallback or errorCallback is called for the final
//                  image to be loaded. If loadCallback or errorCallback returns
//                  true, this callback will be called with imagesHandled and
//                  errors counting only the images that were loaded.

export const ImagePreloadStatus = {unloaded: 0, loading: 1, loaded: 2, errored: 3};
Object.freeze(ImagePreloadStatus);


export class ImagePreloader {
    constructor(urls, args = {}) {
        this.urls = urls;
        this.base = args.base ?? "";
        this.keys = args.keys ?? urls;
        this.loadCallback = args.loadCallback ?? function(preloader, imagesHandled, index) {};
        this.errorCallback = args.errorCallback ?? function(preloader, imagesHandled, index) {};
        this.allCallback = args.allCallback ?? function(preloader, imagesHandled, errors) {};

        this.imageStore = new Object();
        this.isLoading = false;
        this.statuses = new Object();
        this.callbacks = new Object();
        this.isLoading = false;
        this.imagesHandled = 0;
        this.errors = [];

        this.isLoading = true;
        for (let i = 0; i < this.urls.length; i++) {
            this.preloadImage(i);
        }
    }

    // Returns the Image object corresponding to the key provided in the argument
    // key, or null if the image hasn't loaded yet or there was an error in loading
    // it. If the image may not be loaded yet and you want to wait until it is, use
    // doAfterLoaded().
    getImage(key) {
        if (!(this.statuses.hasOwnProperty(key)) || (this.statuses[key] != ImagePreloadStatus.loaded)) {
            return null;
        } else {
            return this.imageStore[key];
        }
    }

    // Calls the function callback after the Image object corresponding to the key
    // provided in the argument key has loaded or failed to load. The function
    // signature should look like this:
    //    function callback(preloader, key, image)
    // preloader is the preloader object, key is the key of the image, and image is
    // either the Image object if it loaded successfully or null if it didn't. If
    // this method was previously called and the callback has not been called yet
    // (so the image is still loading), then the callback will be replaced with the
    // current one. This callback will be called BEFORE loadCallback or
    // errorCallback, if either was provided when the preloader was created.
    doAfterLoaded(callback, key) {
        if (this.statuses.hasOwnProperty(key) && (this.statuses[key] == ImagePreloadStatus.loaded)) {
            callback(this, key, this.imageStore[key]);
        } else {
            this.callbacks[key] = callback;
        }
    }


    // private

    preloadImage(index) {
        let image = new Image();
        let preloader = this;
        image.onload = function() { preloader.imageLoaded(index); };
        image.onerror = function() { preloader.imageErrored(index); };
        image.src = this.base + this.urls[index];
        this.statuses[this.keys[index]] = ImagePreloadStatus.loading;
        this.imageStore[this.keys[index]] = image;
    };

    imageLoaded(index) {
        let key = this.keys[index]
        this.statuses[key] = ImagePreloadStatus.loaded;
        this.imagesHandled += 1;
        if (this.callbacks.hasOwnProperty(key)) {
            this.callbacks[key](this, key, this.imageStore[key]);
        }

        let stopLoading = this.loadCallback(this, this.imagesHandled, index);
        if (stopLoading || this.imagesHandled == this.urls.length) {
            this.finishLoading();
        }
    };

    imageErrored(index) {
        let key = this.keys[index];
        this.statuses[key] = ImagePreloadStatus.errored;
        this.errors[this.errors.length] = key;
        this.imagesHandled += 1;
        if (this.callbacks.hasOwnProperty(key)) {
            this.callbacks[key](this, key, null);
        }

        let stopLoading = this.errorCallback(this, this.imagesHandled, index);
        if (stopLoading || this.imagesHandled == this.urls.length) {
            this.finishLoading();
        }
    };

    finishLoading() {
        this.isLoading = false;
        for (let i = 0; i < this.urls.length; i++) {
            let key = this.keys[i];
            if (this.statuses[key] == ImagePreloadStatus.loading) {
                this.imageStore[key].onload = undefined;
                this.imageStore[key].onerror = undefined;
                delete this.imageStore[key];
                this.statuses[key] = ImagePreloadStatus.unloaded;
            }
        }
        this.allCallback(this, this.imagesHandled, this.errors);
    }

}
