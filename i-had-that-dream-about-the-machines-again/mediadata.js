// mediadata.js
// Image & sound data

export let sounds = [{key: "machine", urls: ["machine.wav"]}, {key: "machineloop", urls: ["machineloop.ogg", "machineloop.mp3"]}, {key: "type", urls: ["type.wav"]}, {key: "receive", urls: ["receive.wav"]}, {key: "send", urls: ["send.wav"]}, {key: "lock", urls: ["lock.wav"]}];

export let music = [{key: "1", urls: ["metroiddread-mapstation2-1.ogg", "metroiddread-mapstation2-1.mp3"]}, {key: "2", urls: ["metroiddread-mapstation2-2.ogg", "metroiddread-mapstation2-2.mp3"]}, {key: "3", urls: ["metroiddread-mapstation2-3.ogg", "metroiddread-mapstation2-3.mp3"]}, {key: "4", urls: ["metroiddread-mapstation2-4.ogg", "metroiddread-mapstation2-4.mp3"]}];
export let musicDefs = {"1": {loopStart: 50}, "2": {loopStart: 50}, "3": {loopStart: 50}, "4": {loopStart: 50}};

let images = [
    {name: "buttons"},
    {name: "camera", retina: true},
    {name: "background", extension: "jpg"}
];

export let imageKeys = [], imageURLs = [];
for (let image of images) {
    let keys = (image.single ?? false) ? [image.name] : [`${image.name}1`, `${image.name}2`, `${image.name}3`, `${image.name}4`];
    if (image.retina ?? false) {
        keys = keys.concat(keys.map(key => `${key}@2x`));
    }
    imageKeys = imageKeys.concat(keys);
    imageURLs = imageURLs.concat(keys.map(key => `${key}.${image.extension ?? "png"}`));

}

export let fonts = ["Roboto", "BreezeSans", "FiraSans", "IBMPlexSans"];
