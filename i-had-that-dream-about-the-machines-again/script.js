﻿// script.js
// Conversation text.


export let prelude = [
    [
        "> new phone who dis",
        "< You messaged me, it doesn't work like that",
        "> im being efficient in pretending i dont know you",
        "< Wouldn't the most efficient way of doing that have been not messaging me at all?",
        "> yea but that wouldnt rub it in your face",
        "< I respect that"
    ],
    [
        "> coolpup69",
        "> shit delete that",
        "< Okay",
        "< Done, it's like I never saw it",
        "> ty",
        "> sup",
        "< Watching some bad movie",
        "> why",
        "< I didn't know it was bad when it started",
        "> but you know its bad now",
        "< Now I'm committed",
        "> sunk cost fallacy a bitch"
    ],
    [
        "> hey i made some cookies i cant take to work",
        "> you want some",
        "< What's wrong with them?",
        "> im not telling",
        "> if youre intrigued youll want them more",
        "< I hate that you're right",
        "> be there after work"
    ],
    [
        "< I threw the rest away as soon as you left",
        "< Now it's between the dumpster out back and god",
        "> who would win that fight",
        "< I honestly don't know",
        "> same"
    ],
    [
        "> i had that dream about the machines again"
    ]
];

export let preludeDesiredTimestamps = [
    { daysAgo: 4, hours: 18, minutes: 52 },
    { daysAgo: 3, hours: 20, minutes: 8 },
    { daysAgo: 2, hours: 8, minutes: 28 },
    { daysAgo: 2, hours: 17, minutes: 49 },
];


export let sections = [
    {
        body: [],
        prompts: ["What dream?", "what dream?"],
        endings: [[],[]]
    },
    {
        body: [
            "> the one i told you about last week",
            "> i know you didnt forget already",
            "< I have amnesia",
            "< Who are you?",
            "< Who am I?",
            "> i hate you so much",
            "< 💛💛",
            "> 💀️",
            "> so like",
            "> i was doing the most boring shit imaginable",
            "> last time i was going to the post office",
            "> this time it was an atm",
            "< Who dreams about going to the post office?",
            "> right???",
            "> imagine youre a brain",
            "> but youre so bad at your job",
            "> that out of all the things in the world you could put into a dream",
            "> the only one you can think of is a fucking post office",
            "> wasting my gd rem cycles",
            "> anyway",
            "> i dont even remember what i was doing at the atm but i did it",
            "> and when im leaving thats when i notice the machines"
        ],
        prompts: ["Like, by the ATM?", "Like, inside the ATM?"],
        endings: [[], []]
    },
    {
        body: [
            "> no like",
            "> on it",
            "> around it?",
            "> hard to describe",
            "> like some 4d shit",
            "> existing tangentially in a different space",
            "> and not just on the atm",
            "> on everything",
            "< Like a factory?",
            "> kind of",
            "> but not really",
            "> more like",
            "> stage machinery",
            "> cranes",
            "> rigging",
            "> but for the whole world",
            "> and i was the only person who could see them"
        ],
        prompts: ["Sounds like that game about the village", "Sounds like that game about the mechanic"],
        endings: [
            [
                "> what game",
                "< I don't know, someone was telling me about it a while ago",
                "< There's a village where an experiment is taking place",
                "< Everyone is hypnotized so they can't see a specific color",
                "> what",
                "< And there are a bunch of machines and operators around town, all draped in that color, so no one can see them",
                "< And they use that invisibility to make everyone think magic is real",
                "> what",
                "> thats fucked up",
                "> what possible reason could there be for that experiment",
                "< Hell if I know",
                "< They didn't tell me"
            ],
            [
                "> what game",
                "< I don't know, someone was telling me about it a while ago",
                "< It takes place on an artificial planet",
                "< There are machines below the surface that control the geography",
                "< They could change desert into ocean, or move mountains",
                "< And the government doesn't want people to find out about the machines, so they ban mapmaking",
                "< Since if people made maps, they would notice when things change because the maps were no longer correct",
                "> couldnt you notice those things without a map",
                "> if i went to bed one night",
                "> and when i woke up",
                "> that mountain over there was no longer there",
                "> i think would notice that",
                "> not 100% sure",
                "> but like 95% sure",
                "< I dunno, I didn't say it was a good plan",
                "< And didn't you say that you were the only one who could see the machines?",
                "< Maybe it's like that",
                "> fair ig"
            ]]
    },
    {
        body: [
            "> why do you remember all that and not the dream i told you about eight days ago",
            "< 🤷",
            "> anyway yeah",
            "> it was kind of like that",
            "> except the machines would like",
            "> replace things",
            "> everything",
            "> constantly",
            "> i walked away from the atm and it changed to a different atm",
            "> and the bank changed to a different bank",
            "> and the other buildings changed to different buildings",
            "> different but the same",
            "> machines creaking in an endless cycle of change"
        ],
        prompts: ["Sounds hypnotic", "Sounds beautiful"],
        endings: [[], []]
    },
    {
        body: [
            "> it was",
            "> but it was also",
            "> idk how to put it",
            "> what would you do with the sudden realization that nothing exists as itself",
            "> there is no atm",
            "> there are the collective atms",
            "> all existing there at the same time",
            "> so whats there isnt an atm",
            "> but rather the abstract idea of an atm",
            "> the abstract idea of a bank",
            "> the abstract idea of a world",
            "> an entire universe of empty wireframes that everyone believes are real",
            "< Probably scream a lot",
            "< Then go back to drawing smut",
            "> so just a typical day for you",
            "< Yeah pretty much",
            "> does the existential horror help with the porn",
            "< Oh absolutely",
            "< I just can't quite get butts right without it",
            "> 🍑",
            "< For real though, it sounds like your subconscious invented a philosophical thought experiment about idealism",
            "> whats that",
            "< Basically the idea the \"real world\" is unknowable, all we can know for sure is our mind and what we perceive the world to be",
            "< We know we're perceiving something, but we can't actually prove it has any relation to some kind of material reality",
            "< Though you've constructed it as a creepypasta scenario where nothing's real because machines keep changing it just to screw with us",
            "> dont trust machines or philosophy",
            "> thats my motto",
            "< Smart",
            "> do you believe in idealism"
        ],
        prompts: ["Kind of, it's a convincing argument", "I don't really believe in anything"],
        endings: [
                    [
                        "< But I don't really read this stuff to figure out what to believe in, I just do it because I think it's interesting",
                        "< If I did, I'd probably have changed my belief system fifty times by now",
                        "> whats wrong with that",
                        "> i change my belief system every day",
                        "> im changing it right now",
                        "< To what?",
                        "> my new belief system is",
                        "> deez",
                        "< If that's the best you could come up with, it's no wonder you change it so often",
                        "> im not going to take this oppression from you"
                    ],
                    [
                        "< I just read philosophical stuff because I think it's interesting, I've never read an argument and been like, \"oh dang, I'm convinced\"",
                        "< Maybe one day",
                        "> i dare you to believe the last philosophy you read",
                        "> ten dollars",
                        "> money on the table",
                        "< I'm reading Aristotle's Politics right now",
                        "< He says slavery is natural because some people are biologically inferior and incapable of rational thought",
                        "< So that's what you're funding",
                        "> gross",
                        "> money off the table",
                        "< You can't just take that back",
                        "> you only perceived there was money on the table",
                        "> you cant prove there was",
                        "< I'd tell you that's not how idealism works but I know you don't care",
                        "> sure dont"
                    ]]
    },
    {
        body: [
            "> anyway it didnt feel like an experiment",
            "> more like a diorama",
            "> everything had machines not just the buildings",
            "> cycling the clouds in the sky",
            "> the trees",
            "> the people",
            "> everything",
            "< Oh, duh",
            "< Machines controlling the flow of existence",
            "< Determinism is the obvious philosophical analogy, I should've led with that",
            "< The obvious question is, were you being constantly replaced by different yous too?",
            "> dunno",
            "> never thought to look in the dream",
            "> im not sure that id want to know",
            "> would you"
        ],
        prompts: ["Of course I would", "Of course I wouldn't"],
        endings: [[], []]
    },
    {
        body: [
            "> oh fuck",
            "> forgot the laundry brb",
            "> dont leave me hanging tho",
            "< Too late, I already forgot my answer",
            "< Pesky amnesia at it again",
            "> why are you like this",
            "< You know exactly what you did",
            "> you right",
            "> and ill never apologize for it",
            "< I'm glad you agree that you deserve this",
            "> weird how i only get the bad things i deserve and not the cool things i deserve",
            "> actually brb tho"
        ],
        prompts: [],
        endings: [[], []]
    }
];
